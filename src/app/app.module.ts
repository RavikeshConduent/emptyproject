import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { from } from 'rxjs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeeFormComponent } from './employee/Employee-Form/employee-form.component';
import { EmployeeListComponent } from './employee/Employee-List/employee-list.component'
import { EmployeeModel } from './shared/employee-model';
import { EmployeeDetailComponent} from './employee/employee-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeFormComponent,
    EmployeeListComponent,
    EmployeeDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [EmployeeModel],
  bootstrap: [AppComponent]
})
export class AppModule { }
